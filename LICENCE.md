All aem-bookmarklet files (except for settings.js) are subjected to my own licence.
All rights belong to Yaci - the original author.
By using or modifying this software you agree to
1. Explain improvements to Yaci and teach him how to make his code better.
2. Not remove "(c) by Yaci" sign from footer.
3. Never claim the authorship of the bookmarklet.

In exchange you can
* Use the bookmarklet for free including commercial purposes.
* Modify and distribute the bookmarklet in any way you want (as long as you abide to points 1-3). 