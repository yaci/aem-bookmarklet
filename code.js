//---object declarations---
function Branch(label,path) {
   this.label = label;
   this.path  = path;
}

function Link(label, href, htmlClass, onclick) {
   this.href = href;
   this.htmlClass = htmlClass;
   this.label = label;
}

function Page() {
   this.domain = "";
   this.ip = "";
   this.urlPattern = "";
   this.branches = {};
}

function Environment(name) {
   this.name = name;
   this.author = new Page();
   this.publish = new Page();
}

function ElementsArray() {
   this.elems = {};
   this.atIndex = function(section, row) {
      if (this.elems[section] === undefined) {
         this.elems[section] = [];
      }
      if (this.elems[section][row] === undefined) {
          this.elems[section][row] = [];
      }
      return this.elems[section][row];
   };
}

var Dimensions = function(rowsPerSection, columns) {
   this.rowsPerSection = rowsPerSection;
   this.sections = Object.keys(rowsPerSection);
   this.columns = columns;
};

//------


function weNeedToRunThisBecauseYaciIsAnIdiotAndThisIsWorkaroundForHisStupidity(envs) {
//I am an idiot and assigned branches to environments forgeting that
//it's reference to an object being assigned, not a copy of it. This resulted in e.g.
//local.publish.branches.demo.domain overriding the value of
//local.author.branches.demo.domain because 'demo' was the same object.
//So in this function we iterate over all environments and their branches, and replace
//every object with its copy.
   let env, envKeys, eKey, branchesList, branchesKeys, bKey, newArray;
   for (env of envs) {
      envKeys = Object.keys(env);
      for (eKey of envKeys) {
          if (env[eKey] instanceof Page) {
            branchesList = env[eKey].branches;
            branchesKeys = Object.keys(branchesList);
            newArray = {};
            for (bKey of branchesKeys) {
               newArray[bKey] = new Branch(branchesList[bKey].label, branchesList[bKey].path);
            }
            env[eKey].branches = newArray;
         }
      }
   }
}



//This is based on obsolete assumption that some rows or columns
//could be empty (e.g. in case when domain was not provided). While
//the assumption changed, the method is left as it was as it provides
//greater flexibility.
//The method calculates the number of columns (environments) for a
//given branch, as well as the humber of rows per each section.
function getDimensions(arr) {
   const sectionNames = Object.keys(arr);
   let rowCountsPerSection = {};
   let maxR = 0, maxC = 0;
   let c, colsCount, sectionName, rowsCount;
   for (sectionName of sectionNames) {
      colsCount = (arr[sectionName]) ? arr[sectionName].length : 0;
      maxC = Math.max(colsCount, maxC);
      maxR = 0;
      for (c=0; c<colsCount; c++) {
         rowsCount = (arr[sectionName][c]) ? arr[sectionName][c].length : 0;
         maxR = Math.max(rowsCount, maxR);
      }
      rowCountsPerSection[sectionName] = maxR;
   }
   return new Dimensions(rowCountsPerSection, maxC);
}

function generateArrayWithLinksThatIsEasierToProcess(arr) {
   let finalArr={};
   let dims = getDimensions(arr.elems);
   let section, r, k;
   for (section of dims.sections) {
      finalArr[section] = [];
      for (r=0; r<dims.rowsPerSection[section]; r++) {
         finalArr[section][r] = [];
         for (k=0; k<dims.columns; k++) {
            if (arr.elems[section] && arr.elems[section][k] && arr.elems[section][k][r]) {
               finalArr[section][r][k] = arr.elems[section][k][r];
               markMisconfiguredCellsAsError(finalArr[section][r][k]);
            }
            else {
               finalArr[section][r][k] = false;
            }
         }
      }
   }
   return finalArr;
}



//---creating elements---

function createCell(link) {
   let div = document.createElement('div');
   div.className = 'col ' + link.htmlClass.join(' ');
   let a = document.createElement('a');
   a.href = link.href;
   a.innerHTML = link.label;
   div.appendChild(a);
   return div;
}

function createDiv(className) {
   let div = document.createElement('div');
   div.className = className;
   return div;
}

function createRow() {
    return createDiv('row');
}

function createShorteningLink() {
   let shorteningButton = document.createElement('input');
   shorteningButton.className = "btnShort";
   shorteningButton.type = "button";
   shorteningButton.value = "s";
   shorteningButton.setAttribute("onclick", "displayShort(this);");
   return shorteningButton;
}

function createEmptyCell() {
   let div = document.createElement('div');
   div.className = 'col empty';
   div.innerHTML = 'x';
   return div;
}

function createSection(section) {
   let div = document.createElement('div');
   div.className = 'section ' + section;
   let headerDiv = document.createElement('div');
   headerDiv.className = 'col sectionHeading ' + section;
   let h = document.createElement('div');
   h.innerHTML = section;
   h.className = section ;
   headerDiv.appendChild(h);
   div.appendChild(headerDiv);
   return div;
}

function createRadio(branchLabel, selected) {
   let input = document.createElement('input');
   input.type = "radio";
   input.name = "branches";
   input.id = "branch-" + branchLabel;
   input.value = branchLabel;
   input.setAttribute("onclick", "generateLinksHtml()");
   if (selected) {
      input.setAttribute("checked", "checked");
   }
   let label = document.createElement('label');
   label.setAttribute("for", "branch-" + branchLabel);
   label.innerHTML = branchLabel;
   let radioWithLabel = document.createDocumentFragment();
   radioWithLabel.appendChild(input);
   radioWithLabel.appendChild(label);
   return radioWithLabel;
}

function insertBranchRadios(branches, selectedBranch) {
   const div = document.getElementById('branches-row');
   const divCol = createDiv('col');
   let branch, radioWithLabel;
   for (branch of branches) {
      let radioWithLabel = createRadio(branch.label, branch === selectedBranch);
      divCol.appendChild(radioWithLabel);
   }
   div.appendChild(divCol);
}


function htmlFragmentWithAllTheLinks(finalArr){
   let cell, rowDiv, section, sections, sectionDiv, sectionLinks, link, row;
   const frag = document.createDocumentFragment();
   sections = Object.keys(finalArr);
   for (section of sections) {
      sectionDiv = createSection(section);
      sectionLinks = document.createElement('div');
      sectionLinks.className = "sectionLinksWrapper";
      for (row of finalArr[section]) {
         rowDiv = createRow();
         for (link of row) {
            if (link) {
               cell = createCell(link);
               if (section === 'publish') {
                  cell.appendChild(createShorteningLink());
               }
               if (link.htmlClass.indexOf('treeActivation') > -1) {
                  registerTreeActivationClick(cell);
               }
            }
            else {
               cell = createEmptyCell();
            }
            rowDiv.appendChild(cell);
         }
         sectionLinks.appendChild(rowDiv);
      }
      sectionDiv.appendChild(sectionLinks);
      frag.appendChild(sectionDiv);
   }
   return frag;
}
//---end of creating elements---
//---other---

function insertOriginalUrl(originalUrl) {
    let a = document.getElementById('originalUrl');
    a.href = originalUrl;
    a.innerHTML = originalUrl;
}

function markMisconfiguredCellsAsError(cellElement) {
   if (cellElement.href.indexOf('ERROR') > -1) {
      cellElement.href = "ERROR (SEE CONSOLE)";
      cellElement.label = "ERROR (SEE CONSOLE)";
      cellElement.htmlClass = cellElement.htmlClass.concat("error");
   }
}

function registerTreeActivationClick(cell) {
   let a = cell.getElementsByTagName('a')[0];
   a.setAttribute("onclick", "activateTree(this); return false;");
}


function activateTree(a) {
   let wrapper = document.getElementById('contentActivationWrapper');
   let input = document.getElementById('contentActivationPath');
   let activationLink = document.getElementById('contentActivationLink');
   const branch = getSelectedBranch();
   input.value =  branch.path + commonPath;
   activationLink.href = a.href;
   wrapper.style.display = "block";
}

function insertTableHeadings(branch) {
   const frag = document.createDocumentFragment();
   const div = document.getElementById('tableHeading');
   div.innerHTML = '';
   let cell, e;
   for (e of envs) {
      if ((branch.label in e.author.branches) || (branch.label in e.publish.branches)) {
         cell = createDiv();
         cell.className = "col";
         cell.innerHTML = e.name;
         frag.appendChild(cell);
      }
   }
   div.appendChild(frag);
}

//This method is for initial generation of links and radio buttons.
//It runs only once when the page loads.
function generateHtml(defaultBranch) {
   const branch = defaultBranch || branches[0];
   const arr = generateUrls(branch);
   const simplerArray = generateArrayWithLinksThatIsEasierToProcess(arr);
   const linksHtml = htmlFragmentWithAllTheLinks(simplerArray);
   insertBranchRadios(branches, branch);  //branches is a global var defined in settings
   const frag = document.createDocumentFragment();
   const linksWrappingDiv = createDiv('linksWrapper');
   linksWrappingDiv.id = 'linksWrapper';
   linksWrappingDiv.appendChild(linksHtml);
   frag.appendChild(linksWrappingDiv);
   document.getElementById('mainWrapper').appendChild(frag);
   insertOriginalUrl(originalUrl);
   insertTableHeadings(branch);
   console.log('generated succesfully');
}

function getBranchByLabel(branchLabel) {
   for (let branch of branches) {
      if (branch.label === branchLabel) {
         return branch;
      }
   }
   return null;
}

function getSelectedBranch() {
   const selectedBranchLabel = document.querySelector('input[name="branches"]:checked').value;
   return getBranchByLabel(selectedBranchLabel);
}

//Generates the new set of links every time a user selects
//a branch by clicking on radio button
function generateLinksHtml() {
   const selectedBranch = getSelectedBranch();
   const arr = generateUrls(selectedBranch);
   const simplerArray = generateArrayWithLinksThatIsEasierToProcess(arr);
   const linksHtml = htmlFragmentWithAllTheLinks(simplerArray);
   const linksWrappingDiv = document.getElementById('linksWrapper');
   linksWrappingDiv.innerHTML = '';
   linksWrappingDiv.appendChild(linksHtml);
   insertTableHeadings(selectedBranch);
}

generateHtml(defaultBranch);

