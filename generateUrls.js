function generateUrls(branch) {
   const arr = new ElementsArray();
   var label, href, elem;
   var htmlClass = [];
   for (let k=0; k<envs.length; k++) {
      let e = envs[k];
      let envName = e.name;
      let doAuthor = (branch.label in e.author.branches);
      let doPublish = (branch.label in e.publish.branches);
      //author section
      if (doAuthor)  {
         let commonClasses = [envName, "author", branch.label];
         let commonLabel = envName + " author " + branch.label;
         let domain = getDomain(e, 'author', branch);
         //
         label = commonLabel;
         htmlClass = ["editMode", "touch"];
         href = domain + "/editor.html" + branch.path + commonPath + ".html";
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('author', k).push(elem);
         //
         label = commonLabel + " preview";
         htmlClass = ["preview"];
         href = domain + branch.path + commonPath + ".html";
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('author', k).push(elem);
         //
         label = commonLabel + " page selector";
         htmlClass = ["pageSelector"];
         href = domain + "/sites.html" + branch.path + commonPath;
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('author', k).push(elem);
      }

      //publish
      if (doPublish) {
         //arr['publish'][k] = []; //czy takie cos nie wystarcza zamiast custom array?!!??!
         htmlClass = [envName, "publish", branch.label];
         let pattern = getPattern(e, 'publish', branch);
         let domain = getDomain(e, 'publish', branch);
         pattern = pattern.replace("${domain}", domain)
                          .replace("${branchPath}", branch.path)
                          .replace("${commonPath}", commonPath);
         href = pattern;
         label = envName + " publish " + branch.label;
         elem = new Link(label, href, htmlClass);
         arr.atIndex('publish', k).push(elem);
      }

      //other (publish)
      if (doPublish && e.publish.ip && showOther) {
         //arr['other'][k] = [];
         label = envName + " publish CRX " + branch.label;
         htmlClass = [envName, "publish", "crx", "other", branch.label];
         href = e.publish.ip + "/crx/de/index.jsp#" + branch.path + commonPath;
         elem = new Link(label, href, htmlClass);
         arr.atIndex('other', k).push(elem);
      }
      
      //other (author)
      if (doAuthor && e.author.ip && showOther) {
         let commonClasses = [envName, "author", "other", branch.label];
         let domain = e.author.branches[branch.label].domain || e.author.domain;
         //
         label = envName + " author CRX " + branch.label;
         htmlClass = ["crx"];
         href = e.author.ip + "/crx/de/index.jsp#" + branch.path + commonPath;
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('other', k).push(elem);
         //
         label = envName + " page selector (old) " + branch.label;
         htmlClass = ["pageSelectorOld"];
         href = domain + "/siteadmin#" + branch.path + commonPath;
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('other', k).push(elem);
         //
         label = envName + " tree activation " + branch.label;
         htmlClass = ["treeActivation"];
         href = domain + "/etc/replication/treeactivation.html";
         elem = new Link(label, href, commonClasses.concat(htmlClass));
         arr.atIndex('other', k).push(elem);
      }
   }
   return arr;
}

//-----do not modify anything below this line-----

function getProperty(e, instance, branch, property) {
   let propValue = e[instance].branches[branch.label][property] || e[instance][property];
   if (propValue) {
      return propValue;
   }
   else {
      let instanceString = e.name + "." + instance + ".";
      console.log(property + " not found in neither " + instanceString + branch.label + "." + property + " nor in " + instanceString + property + "\nPlease set one of those properties.");
      return "ERROR (see console)";
   }
}

function getDomain(e, instance, branch) {
   return getProperty(e, instance, branch, 'domain');
}

function getPattern(e, instance, branch) {
   return getProperty(e, instance, branch, 'urlPattern');
}



