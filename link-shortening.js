'use strict';
//----link shortening----
// This file is loaded independently, you do NOT need to run merge script
// after making changes to this file.
//
// Link shortener domain can be set in settings.js file (near the bottom)

function shortenLink(longUrl, callbackFunction) {
   let apiUrl = linkShortenerApiDomain + '/yourls-api.php';
   let params = "signature="+linkShortenerApiKey+"&action=shorturl&format=json&url=" + encodeURIComponent(longUrl);
   let xhr = new XMLHttpRequest();
   xhr.open('POST', apiUrl, true);
   xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
   xhr.onload = function () {
      if (xhr.status === 200) {
         callbackFunction(this.responseText);
      }
      else {
         displayError("Server responded with " + xhr.status);
      }
   };
   xhr.onerror = function() {
      displayError("Request failed (is your internet connection alive?)");
   };
   xhr.send(params);
}

function displayError(message) {
   document.getElementById('shortUrlText').value = "error (see console)";
   message = "An error occured when trying to connect to link shortening api :(\n" + message;
   console.log(message);
}

//this function is passed as a callbackFunction parameter to shortenLink
var displayQrAndShortenedLink = function(responseText) {
   let jsonResponse = JSON.parse(responseText);
   let shortenedUrl = jsonResponse.shorturl;
   let qrSrc = jsonResponse.qrcimg;
   shortenedUrl = shortenedUrl.replace(/https?:\/\//, '');
   document.getElementById('shortUrlText').value = shortenedUrl;
   document.getElementById('qrImg').src = qrSrc;
};

//this function is invoked by clicking on "shorten" button
function displayShort(shortButton) {
   let a = shortButton.previousSibling;
   let shortUrlWrapper = document.getElementById('shortUrlWrapper');
   document.getElementById('longurl').innerHTML = a.href + " (" + a.innerHTML + ")";
   shortenLink(a.href, displayQrAndShortenedLink);
   shortUrlWrapper.style.display = "block";
}