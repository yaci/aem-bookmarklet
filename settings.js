"use strict";
//-------------config--------------------

//1. Define environments
//   Environment means integration, staging etc.
const local       = new Environment("local");
const dev         = new Environment("dev");
const preprod     = new Environment("preprod");
const prod        = new Environment("prod");

//2. Make a list of newly created environments and assign it to 'envs' variable
const envs = [local, dev, preprod, prod];

//3. Define content/language brenches
const master = new Branch("master", "/content/project/demo/en/home");
const global = new Branch("global", "/content/project/global/en/home");
const demo   = new Branch("demo",   "/content/project/demo/en_us/home");

//4. Make a list of newly created branches and assign it to 'branches' variable
const branches = [master, global, demo];

//5. Define which branches are available on each environment
//   If you followed above steps you should be able to access
//   $environmentName.author.branches and
//   $environmentName.publish.branches
//   Assign list of branches to each of them
local.author.branches    = {master, global, demo};
local.publish.branches   = {master, global, demo};
dev.author.branches      = {master, global, demo};
dev.publish.branches     = {global, demo};
preprod.author.branches  = {global, demo};
preprod.publish.branches = {global, demo};
prod.publish.branches    = {global};

//6. Run workaround for author's mental disability (sorry for that)
weNeedToRunThisBecauseYaciIsAnIdiotAndThisIsWorkaroundForHisStupidity(envs);

//7. Set author domain and IP for each environment.
//   It is assumed that domain is the same regardless of the branch.
//   Include protocol (a must) and port number (if necessary) for both
//   domain and ip properties.
//   If you are using ip instead of domain name to access an instance,
//   it's necessary to assing the ip to both properties.
//   Do NOT include trailing slash.
//   Examples:
//    - accessing local instance using IP
//        local.author.domain = "http://127.0.0.1:4502"
//        local.author.ip     = "http://127.0.0.1:4502"
//    - integration env accessible via domain, but crx via ip
//        integration.author.domain = "https://author-int-project.cognifide.com
//        integration.author.ip     = "https://10.0.0.1:4502"
local.author.domain   = "https://author-project.vagrant";
local.author.ip       = "http://127.0.0.1:4502";

dev.author.domain     = "https://author-dev.project.com";

preprod.author.domain = "http://10.0.0.1:4502";
preprod.author.ip     = "http://10.0.0.1:4502";

//8. Set publish domain and IP for each environment
//   The principles are the same as for author domains (point 7 above)
//   with one major exception.
//   Since project may be using different domains for each content/language branch
//   you may need to specify a domain for each of the branches using
//   $environmentName.publish.branches.$branchName.domain where
//   $branchName are names of the branches that you have created in points 3-5.
//   This syntax will be available for each of the branches that you have assigned
//   to environment. In other words if you have not assigned for instance demo branch
//   to uat environment you will NOT be able to access uat.publish.branches.demo
//   IPs however are one per environment, you set them once per all branches on
//   given environment.
//   NOTE: ip means protocol+ip+port (e.g. https://127.0.0.1:4503)
//   IMPORTANT: setting domain name for a branch will override the "global" setting
//   (see example a.)
//   Examples:
//     a. Let's assume we have 3 branches: america, demo and china.
//        In the following example, for demo branch the domain is set to
//        http://demo-project.vagrant however for america and china branch
//        the domain remains "http://project.vagrant"
//
//          local.publish.domain = "http://project.vagrant"
//          local.publish.branches.demo.domain = "http://demo-project.vagrant"
//          local.publish.ip = "http://127.0.0.1:4503"
//
//     b. All domains set separately for each branch. "Global" setting is not
//        needed in such case
//
//          prod.publish.branches.america = "https://en.project.com"
//          prod.publish.branches.china   = "https://ch.project.com"
//          prod.publish.branches.demo    = "https://demo.project.com"
//
local.publish.ip                      = "http://127.0.0.1:4503";
local.publish.branches.master.domain  = "http://127.0.0.1:4503";
local.publish.branches.global.domain  = "https://project.vagrant";
local.publish.branches.demo.domain    = "https://demo-project.vagrant";

dev.publish.ip                     = "https://10.0.0.1:4502";
dev.publish.branches.global.domain = "https://dev.project.com";
dev.publish.branches.demo.domain   = "https://dev-demo.project.com";

preprod.publish.branches.global.domain = "https://preprod.project.com";

prod.publish.branches.global.domain = "https://www.project.com";

//9. Set url pattern for each of the branches on each environment
//   Url building for the author is simple. We take domain+branch path+rest of url+.html
//   to build the address.
//   However it's different for publish. Your project may leave '.html' suffixes on
//   some instances and remove them on the others. Also, you may choose for your urls
//   to end with trailing slash or not to have an ending at all.
//   Because of so many factors that may be difficult to predict, you have to specify
//   a pattern for each instance, indicating how the final url is supposed to look like.
//   You may use the following variables in the pattern:
//     ${domain}     - the domain you have set in point 8
//     ${branchPath} - the path that you have set in point 3
//     ${commonPath} - the rest of url
//   Examples:
//     a. One pattern for all branches
//
//       local.publish.urlPattern = "${domain}${branchPath}${commonPath}.html
//
//     b. Different pattern for demo domain, and another for the rest. Remaining
//        branches have mappings enabled and end with trailing slash.
//
//       integration.publish.urlPattern = ${domain}{$branchPath}${commonPath}.html
//       integration.publish.branches.demo.urlPattern = ${domain}${commonPath}/
//
//

local.publish.urlPattern   = "${domain}${commonPath}/";
dev.publish.urlPattern     = "${domain}${commonPath}/";
preprod.publish.urlPattern = "${domain}${commonPath}/";
prod.publish.urlPattern    = "${domain}${commonPath}/";

//10. Provide a regexp that matches a branchPath in author url, but not commonPath
//    This will usually start with 'content' and will end with whatever word occurs
//    before commonPath starts.
//    Do NOT escape slashes (like this: \/) - it's done automatically.
//    MUST end with trailing slash, but must NOT be preceded by one!!
//    The final regexp can be seen below, to give you an overview how the commonPath is
//    extracted and how the branchRegex will fit into the bigger regex. However please
//    DO NOT modify the regex itself!!
//    Example:
//      const branchRegex = "content/.+?/home/

const branchRegex = "content/.+?/home/";

//11. Set link shortener domain and api key.
//    For domain include protocol, but do NOT include trailing slash.
//    Example:
//      var linkShortenerApiDomain = "https://gxx.pl"
//      var linkShortenerApiKey    = "2e9825dec7"

const linkShortenerApiDomain = "https://gxx.pl";
const linkShortenerApiKey    = "2e9825dec7";

//12. Define whether the "other" section should be displayed in html.
//    Accepted values: true/false;
const showOther = true;

//13. Define default branch. The results for this branch will be initially shown
//    when you run the bookmarklet.

const defaultBranch = demo;

//13. DONE!
//    IMPORTANT: run merge script to apply the new settings!!!!!!



//DO NOT MODIFY THIS!!
var regex = "^https?://(.+?)/(?:(sites\\.html|editor\\.html|crx/de/index.jsp#|cf#|siteadmin#)/)?("+branchRegex+")?(.*?)(?:(?:\\.html|/)?(?:\\?.*)?)$";




//URLSearchParameters is not supported in IE/Edge so we need to extract parameter manually
const originalUrl = decodeURIComponent(location.search.match(/originalUrl=(.+)/)[1]);
regex = regex.replace(/\//g, "\\/"); //escape slashes
regex = new RegExp(regex);

const match = regex.exec(originalUrl);
//group 1 - domain without protocol
//group 2 - can be empty or one of the following: editor.html, sites.html, siteadmin#, cf#, crx/de
//group 3 - branch path (can be empty)
//group 4 - common path, the part that is independent regardless of branch or domain. This is what we need!

var commonPath;
var originalDomain;

if (match !== null && match !== undefined ) {
   commonPath = match[4];
   if (commonPath) {
       commonPath = "/" + commonPath;  //add preceding slash if path is not empty
   }
   originalDomain = match[1];
}
else {
   commonPath = "ERROR (see console)";
   let errorMesssage = "Could not match original url with commonPath extracting regexp!";
   errorMesssage += "Either originalUrl parameter is wrong or not provided or ";
   errorMesssage += "something is wrong with your regexp.";
   console.log(errorMesssage);
}


//-----------------------

